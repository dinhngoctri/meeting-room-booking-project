const TelegramService = require("../../services/telegram.service");

const newMeetingNotify = () => {
  return async (req, res, next) => {
    try {
      const { bookingId } = req.body;
      await TelegramService.newMeetingNotify(bookingId);
      res.status(200).json({ message: "Đã thông báo." });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = newMeetingNotify;
