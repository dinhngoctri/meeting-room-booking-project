const MeetingService = require("../../services/meeting.service");

const recurringMeetingValidation = () => {
  return async (req, res, next) => {
    try {
      const data = await MeetingService.recurringMeetingValidation(req.body);
      res.status(200).json({ data });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = recurringMeetingValidation;
