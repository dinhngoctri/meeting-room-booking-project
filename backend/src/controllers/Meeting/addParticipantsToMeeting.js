const MeetingService = require("../../services/meeting.service");

const addParticipantsToMeeting = () => {
  return async (req, res, next) => {
    try {
      req.body.meetingHostId = res.locals.user.id;
      const data = await MeetingService.addParticipantsToMeeting(req.body);
      res.status(200).json({ data });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = addParticipantsToMeeting;
