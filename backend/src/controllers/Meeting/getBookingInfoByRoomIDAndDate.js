const MeetingService = require("../../services/meeting.service");
const getBookingInfoByRoomIDAndDate = () => {
  return async (req, res, next) => {
    try {
      const data = await MeetingService.getBookingInfoByRoomIDAndDate(req.body);
      res.status(200).json({ data, message: "Get booking list success" });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = getBookingInfoByRoomIDAndDate;
