const MeetingService = require("../../services/meeting.service");

const cancelBooking = () => {
  return async (req, res, next) => {
    try {
      req.body.userId = res.locals.user.id;
      req.body.role = res.locals.user.role;
      await MeetingService.cancelBooking(req.body);
      res.status(200).json({ message: "Cancel success" });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = cancelBooking;
