const { getUserListInParticipant } = require("../../services/meeting.service");

const getUserListInParticipantController = () => {
  return async (req, res, next) => {
    try {
      const { id } = req.params;
      const result = await getUserListInParticipant(id);
      res.status(200).json({
        message: "take list success",
        result,
      });
    } catch (error) {
      next(error);
    }
  };
};



module.exports = getUserListInParticipantController;
