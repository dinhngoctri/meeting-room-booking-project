const MeetingService = require("../../services/meeting.service");

const userLeavesTheMeetingRoomController = () => {
  return async (req, res, next) => {
    try {
      const result = await MeetingService.userLeavesTheMeetingRoom(req);
      res.status(200).json({
        message: "user leave meeting success",
        result,
      });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = userLeavesTheMeetingRoomController;
