const MeetingService = require("../../services/meeting.service");

const listOfUsersPresentInTheMeetingController = () => {
  return async (req, res, next) => {
    try {
      const { id } = req.params;
      const result = await MeetingService.getUserListInParticipant(id);
      res.status(200).json({
        message: "take list success",
        result,
      });
    } catch (error) {
      next(error);
    }
  };
};



module.exports = listOfUsersPresentInTheMeetingController;
