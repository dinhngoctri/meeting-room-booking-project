const { deleteUserOutOfMeeting } = require("../../services/meeting.service");

const deleteUserOutOfMeetingController = () => {
  return async (req, res, next) => {
    try {
      const result = await deleteUserOutOfMeeting(req);
      res.status(200).json({
        message: "deleted success",
        result,
      });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = deleteUserOutOfMeetingController;
