const MeetingService = require("../../services/meeting.service");

const getDetailMeetingController = () => {
  return async (req, res, next) => {
    try {
      const { id } = req.params;
      const result = await MeetingService.getDetailMeeting(id);
      res.status(200).json({
        message: "take list success",
        result,
      });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = getDetailMeetingController;
