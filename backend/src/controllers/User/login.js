const UserService = require("../../services/user.service");

const login = () => {
  return async (req, res, next) => {
    try {
      const token = await UserService.login(req.body);
      res.status(200).json({ data: { token }, message: "Login success." });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = login;
