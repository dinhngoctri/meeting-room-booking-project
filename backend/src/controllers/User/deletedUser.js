const UserService = require("../../services/user.service");

const deletedUser = () => {
  return async (req, res, next) => {
    try {
      const { id } = req.params;
      let data = await UserService.deletedUser(id, req);
      res.status(200).json({
        message: "deleted success",
        data,
      });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = deletedUser;
