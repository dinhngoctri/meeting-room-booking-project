const UserService = require("../../services/user.service");

const paginateUserController = () => {
  return async (req, res, next) => {
    try {
      const data = await UserService.paginateUser(req);
      res.status(200).json({
        message: "take a list of user success :)",
        data,
      });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = paginateUserController;
