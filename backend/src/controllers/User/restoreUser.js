const UserService = require("../../services/user.service");

const restoreUser = () => {
  return async (req, res, next) => {
    try {
      const { id } = req.params;
      let data = await UserService.restoreUser(id, req);
      res.status(200).json({
        message: "restore success",
        data,
      });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = restoreUser;
