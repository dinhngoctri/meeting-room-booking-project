const UserService = require("../../services/user.service");

const changePassword = () => {
  return async (req, res, next) => {
    try {
      const { id } = req.params;
      const { currentPassword, newPassword, confirmPassword } = req.body;
      let data = await UserService.changePassword(
        id,
        currentPassword,
        newPassword,
        confirmPassword
      );
      res.status(200).json({
        message: "update success",
        data,
      });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = changePassword;
