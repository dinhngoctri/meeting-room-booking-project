const UserService = require("../../services/user.service");

const getListOfUser = () => {
  return async (req, res, next) => {
    try {
      const data = await UserService.takeAListOfUserNotDelelte(req);
      res.status(200).json({
        message: "take a list of user success :)",
        data,
      });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = getListOfUser;
