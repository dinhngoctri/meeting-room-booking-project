const UserService = require("../../services/user.service");

const searchUserController = () => {
  return async (req, res, next) => {
    try {
      const data = await UserService.searchUser(req);
      res.status(200).json({
        message: "take a list of user success :)",
        data,
      });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = searchUserController;
