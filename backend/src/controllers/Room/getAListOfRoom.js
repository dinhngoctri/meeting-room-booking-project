const RoomService = require("../../services/room.service");

const getAListOfRoom = () => {
  return async (req, res, next) => {
    try {
      const data = await RoomService.getAListOfRoom();
      res.status(200).json({ message: "Get room list success.",data });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = getAListOfRoom;
