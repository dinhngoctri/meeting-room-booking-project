const RoomService = require("../../services/room.service");

const deletedRoom = () => {
  return async (req, res, next) => {
    try {
      const { id } = req.params;
      let data = await RoomService.deletedRoom(id, req);
      res.status(200).json({
        message: "update success",
        data,
      });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = deletedRoom;
