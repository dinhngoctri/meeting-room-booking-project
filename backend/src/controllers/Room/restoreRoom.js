const RoomService = require("../../services/room.service");

const restoreRoom = () => {
  return async (req, res, next) => {
    try {
      const { id } = req.params;
      let data = await RoomService.restoreRoom(id);
      res.status(200).json({
        message: "update success",
        data,
      });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = restoreRoom;
