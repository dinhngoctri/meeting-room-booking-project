const RoomService = require("../../services/room.service");

const createRoom = () => {
  return async (req, res, next) => {
    try {
      let data = await RoomService.createRoom(req);
      res.status(200).json({
        message: "create success",
        data,
      });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = createRoom;
