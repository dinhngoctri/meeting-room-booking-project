const RoomService = require("../../services/room.service");

const getRoomList = () => {
  return async (req, res, next) => {
    try {
      const data = await RoomService.getRoomList();
      res.status(200).json({ data, message: "Get room list success." });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = getRoomList;
