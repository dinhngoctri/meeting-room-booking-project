const bookingMeetingToGoogleCalendar = require("../../services/bookingMeetingToGooleCalendar.service");

const deletedEventGoogleCalendar = () => {
  return async (req, res, next) => {
    try {
      const result =
        await bookingMeetingToGoogleCalendar.deletedEventToGoogleCalendar(req,next);
      res.status(200).json({
        message: "Xóa Sự Kiện Thành Công",
        eventId: result,
      });
    } catch (error) {
      console.log(error);
      next(error);
    }
  };
};

module.exports = deletedEventGoogleCalendar;
