const BookingInfoService = require("../../services/bookingInfo.service");

const getDateList = () => {
  return async (req, res, next) => {
    try {
      const data = await BookingInfoService.getDateList(req);
      res.status(200).json({
        message: "take a list of date success",
        data,
      });
    } catch (error) {
      console.log(error);
      next(error);
    }
  };
};

module.exports = getDateList;
