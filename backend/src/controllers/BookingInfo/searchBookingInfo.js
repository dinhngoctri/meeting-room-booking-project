const BookingInfoService = require("../../services/bookingInfo.service");

const searchBookingInfoController = () => {
  return async (req, res, next) => {
    try {
      const data = await BookingInfoService.searchBookingInfo(req);
      res.status(200).json({
        message: "take a list of user success :)",
        data,
      });
    } catch (error) {
      console.log(error);
      next(error);
    }
  };
};

module.exports = searchBookingInfoController;
