const BookingInfoService = require("../../services/bookingInfo.service");

const addGuestAccount = () => {
  return async (req, res, next) => {
    try {
      const data = await BookingInfoService.addGuestAccount(req);
      res.status(200).json({
        message: "thêm thành công",
        data,
      });
    } catch (error) {
      console.log(error);
      next(error);
    }
  };
};

module.exports = addGuestAccount;
