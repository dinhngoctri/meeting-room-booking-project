const bookingMeetingToGoogleCalendar = require("../../services/bookingMeetingToGooleCalendar.service");

const deleteGuestOutOfMeeting = () => {
  return async (req, res, next) => {
    try {
      const data = await bookingMeetingToGoogleCalendar.deleteGuestOutOfMeeting(
        req,next
      );
      res.status(200).json({
        message: "Xóa khách ra khỏi sự kiện thành công",
        data,
      });
    } catch (error) {
      console.log(error);
      next(error);
    }
  };
};

module.exports = deleteGuestOutOfMeeting;
