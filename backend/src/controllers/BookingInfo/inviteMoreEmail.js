const bookingMeetingToGoogleCalendar = require("../../services/bookingMeetingToGooleCalendar.service");

const inviteMoreGuest = () => {
  return async (req, res, next) => {
    try {
      const data = await bookingMeetingToGoogleCalendar.inviteMoreGuest(req);
      res.status(200).json({
        message: "Mời người dùng vào sự kiện thành công",
        data,
      });
    } catch (error) {
      console.log(error);
      next(error);
    }
  };
};

module.exports = inviteMoreGuest;
