const bookingMeetingToGoogleCalendar = require("../../services/bookingMeetingToGooleCalendar.service");

const createEventToGoogleCalendar = () => {
  return async (req, res, next) => {
    try {
      const result =
        await bookingMeetingToGoogleCalendar.createEventToGoogleCalendar(req);
      res.status(200).json({
        message: "Tạo Sự Kiện Thành Công",
        result,
      });
    } catch (error) {
      console.log(error);
      next(error);
    }
  };
};

module.exports = createEventToGoogleCalendar;
