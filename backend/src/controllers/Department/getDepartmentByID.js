const DepartmentService = require("../../services/department.service");

const getDepartmentByID = () => {
  return async (req, res, next) => {
    try {
      const { departmentId } = req.params;
      const data = await DepartmentService.getDepartmentByID(departmentId);
      res.status(200).json({ data, message: "Get department success." });
    } catch (error) {
      next(error);
    }
  };
};

module.exports = getDepartmentByID;
