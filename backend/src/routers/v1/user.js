const express = require("express");
const userRouter = express.Router();
const { authorization, authorizationAdmin } = require("../../middlewares/authorization");

const register = require("../../controllers/User/register");
userRouter.post("/register", register());

const login = require("../../controllers/User/login");
userRouter.post("/login", login());

const verify = require("../../controllers/User/verify");
userRouter.get("/verify", authorization, verify());

// ?------------------------ Admin -------------------------

const getListOfUser = require("../../controllers/User/takeAListOfUser");
userRouter.get("/takeAListOfUser", authorizationAdmin, getListOfUser());

const takeAListOfUserDelelteController = require("../../controllers/User/takeAListOfUserDelete");
userRouter.get(
  "/takeAListOfUserDeleted",
  authorizationAdmin,
  takeAListOfUserDelelteController()
);

const addUserInListController = require("../../controllers/User/addUserInList");
userRouter.post("/addUser", authorizationAdmin, addUserInListController());

const PutUpdateUser = require("../../controllers/User/editUser");
userRouter.put("/update-user/:id", authorizationAdmin, PutUpdateUser());

const deletedUser = require("../../controllers/User/deletedUser");
userRouter.delete("/deleted-user/:id", authorizationAdmin, deletedUser());

const restoreUser = require("../../controllers/User/restoreUser");
userRouter.put("/restore-user/:id", authorizationAdmin, restoreUser());

const searchUserController = require("../../controllers/User/searchUser");
userRouter.get("/search-user", searchUserController());

const deleteAlotOfUserController = require("../../controllers/User/deletedALotOfUser");
userRouter.put("/deleted-Alotof-User", deleteAlotOfUserController());

// ?-------------------------- User ---------------------------------
const changePassword = require("../../controllers/User/changePassword");
userRouter.put("/change-password/:id", changePassword());

const getUserListByDepartmentController = require("../../controllers/User/getUserListByDepartment");
userRouter.post("/getUserByDepartment", getUserListByDepartmentController());

const paginateUserController = require("../../controllers/User/paginateUser");
userRouter.get("/get-list-user-by-page", paginateUserController());

const accessByDCore = require("../../controllers/User/accessByDCore");
userRouter.post("/accessByDCore", accessByDCore());

const findUserByRefId = require("../../controllers/User/findUserByRefId");
userRouter.get("/findUserByRefId/refId-:refId", findUserByRefId());

module.exports = userRouter;
