const express = require("express");
const bookingInfoRouter = express.Router();
const { authorization } = require("../../middlewares/authorization");

const getBookingList = require("../../controllers/BookingInfo/getBookingList");
bookingInfoRouter.get("/getBookinginfoList", authorization, getBookingList());

const getListOfMeetingsYouAttendController = require("../../controllers/BookingInfo/getListOfMeetingsYouAttend");
bookingInfoRouter.get(
  "/getListOfMeetingYouAttend",
  getListOfMeetingsYouAttendController()
);

const deletedBookingInfo = require("../../controllers/BookingInfo/deletedBookingInfo");
bookingInfoRouter.put("/deleted/:id", deletedBookingInfo());

const sendEmailBookinInfo = require("../../controllers/BookingInfo/sendEmailBookingInfo");
bookingInfoRouter.post("/sendEmail", sendEmailBookinInfo());

const checkSendEmailSuccessController = require("../../controllers/BookingInfo/checkSendEmailSuccess");
bookingInfoRouter.put("/checkSendEmail", checkSendEmailSuccessController());

const saveEmailToParticipantsController = require("../../controllers/BookingInfo/saveEmailToParticipantsController");
bookingInfoRouter.post(
  "/saveUserToParticipant",
  saveEmailToParticipantsController()
);

const searchBookingInfoController = require("../../controllers/BookingInfo/searchBookingInfo");
bookingInfoRouter.get("/searchBookingInfo", searchBookingInfoController());

const searchBookingInfoForAdminController = require("../../controllers/BookingInfo/searchBookingInfoForAdmin");
bookingInfoRouter.get(
  "/searchBookinginfoAdmin",
  searchBookingInfoForAdminController()
);

const addGuestAccount = require("../../controllers/BookingInfo/addGuestAccount");
bookingInfoRouter.post("/addGuestAccount", addGuestAccount());

const getDateList = require("../../controllers/BookingInfo/getDateList");
bookingInfoRouter.post("/getDateList", getDateList());

// -----------------------booking meeting to gg calendar--------------------
const createEventToGoogleCalendar = require("../../controllers/BookingInfo/createEventToGoogleCalendar");
bookingInfoRouter.post(
  "/bookingMeetingToGoogleCalendar",
  createEventToGoogleCalendar()
);

const deletedEventGoogleCalendar = require("../../controllers/BookingInfo/deletedEventToGoogleCalendar");

bookingInfoRouter.delete(
  "/deletedEventToGoogleCalendar/:id",
  deletedEventGoogleCalendar()
);

const inviteMoreGuest = require("../../controllers/BookingInfo/inviteMoreEmail");
bookingInfoRouter.put("/inviteMoreGuest", inviteMoreGuest());

const deleteGuestOutOfMeeting = require("../../controllers/BookingInfo/deleteGuestOutOfMeeting");
bookingInfoRouter.delete("/deleteGuestOutOfMeeting", deleteGuestOutOfMeeting());
module.exports = bookingInfoRouter;
