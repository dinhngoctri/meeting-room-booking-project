const express = require("express");
const bookingInfoRouter = require("./bookingInfo");
const departmentRouter = require("./deparment");
const roomRouter = require("./room");
const userRouter = require("./user");
const meetingRouter = require("./meeting");
const notificationRouter = require("./notification");

const rootRouter = express.Router();

rootRouter.use("", userRouter);
rootRouter.use("/department", departmentRouter);
rootRouter.use("/room", roomRouter);
rootRouter.use("/meeting", meetingRouter);
rootRouter.use("/bookinginfo", bookingInfoRouter);
rootRouter.use("", notificationRouter);

module.exports = rootRouter;
