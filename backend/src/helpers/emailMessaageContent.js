const { formatTime } = require("./formatTime");
const { replaceStringInHmtl } = require("./replaceStringInHtml");
const { Room, User } = require("../models/index");
const fs = require("fs");

const emailMessageContent = async (
  bookingDetailUnderData,
  req,
  status,
  subject
) => {
  let { emailList, user, bookingId } = req;
  const { id, startingTime, endingTime, meetingContent, roomId } =
    bookingDetailUnderData;

  const room = await Room.findOne({
    where: { id: roomId },
    attributes: ["roomName"],
    raw: true,
  });

  const startingTimeMoment = formatTime(startingTime, "hh:mm");
  const endingTimeMoment = formatTime(endingTime, "hh:mm");
  const date = formatTime(startingTime, "DD/MM/YYYY");

  let html = fs.readFileSync("./views/pages/index.html", "utf8");

  html = replaceStringInHmtl(
    "{meetingContent}",
    html,
    meetingContent,
    "không có tiêu đề"
  );

  html = replaceStringInHmtl("{date}", html, date);

  html = replaceStringInHmtl("{startingTimeMoment}", html, startingTimeMoment);

  html = replaceStringInHmtl("{endingTimeMoment}", html, endingTimeMoment);

  html = replaceStringInHmtl("{roomName}", html, room.roomName);

  // html = replaceStringInHmtl("{id}", html, bookingId, id);

  html = replaceStringInHmtl("{statusMeeting}", html, status);

  emailList = emailList.map((item) => item.email);

  console.log("emailList", emailList);

  const options = {
    to: `ddv_meeting@didongviet.vn`,
    cc: emailList,
    subject: `DDV 🚀 - ${subject}`,
    text: "This email is sent from the DDV",
    html,
    textEncoding: "base64",
    headers: [
      { key: "X-Application-Developer", value: "Amit Agarwal" },
      { key: "X-Application-Version", value: "v1.0.0.2" },
    ],
  };

  return options;
};

module.exports = emailMessageContent;
