const jwt = require("jsonwebtoken");

const ExpiresIn = 60 * 60 * 12 * 30 * 12;

const generateToken = (payload) => {
  const token = jwt.sign(
    {
      id: payload.id,
      email: payload.email,
      role: payload.role,
      departmentId: payload.departmentId,
      fullname: payload.fullname,
    },
    "didongviet",
    {
      expiresIn: ExpiresIn,
    }
  );

  return token;
};

module.exports = { generateToken };
