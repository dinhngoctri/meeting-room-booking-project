const { Op } = require("sequelize");
const moment = require("moment");
const { AppError } = require("../helpers/error");

const validation = {
  isEmpty(input) {
    if (input == null || !input.trim()) {
      return true;
    }
    return false;
  },
  isNumber(input) {
    if (typeof input === "number") {
      return true;
    }
    return false;
  },
  isEmail(input) {
    const emailRegex =
      /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    return input.match(emailRegex);
  },

  async isExisted(payload) {
    const { model, attribute, value } = payload;
    try {
      const data = await model.findOne({
        where: { [attribute]: value },
      });
      return data;
    } catch (error) {
      return false;
    }
  },

  // async isValidTimeFrame(payload) {
  //   const { model, roomId, startingTime, endingTime } = payload;
  //   return await model.findAll({
  //     where: {
  //       roomId,
  //       status: {
  //         [Op.ne]: "cancel",
  //       },
  //       [Op.or]: [
  //         {
  //           [Op.and]: [
  //             { startingTime: { [Op.lte]: startingTime } }, // existStarting <= createStarting
  //             { endingTime: { [Op.gt]: startingTime } }, // existEnding > createStarting
  //           ],
  //         },
  //         {
  //           [Op.and]: [
  //             { startingTime: { [Op.lt]: endingTime } }, // existStarting < createEnding
  //             { endingTime: { [Op.gte]: endingTime } }, // existEnding >= createEnding
  //           ],
  //         },
  //         {
  //           [Op.and]: [
  //             { startingTime: { [Op.gt]: startingTime } }, // existStarting > createStarting
  //             { endingTime: { [Op.lt]: endingTime } }, // existEnding < createEnding
  //           ],
  //         },
  //         {
  //           [Op.and]: [
  //             { startingTime }, // existStarting = createEnding
  //             { endingTime }, // existEnding = createEnding
  //           ],
  //         },
  //       ],
  //     },
  //     raw: true,
  //   });
  // },

  async isValidTimeFrame(payload) {
    const {
      model,
      // startingTime: createStarting,
      // endingTime: createEnding,
      roomId,
      timeCheckedArray,
      daysCheckedArray,
    } = payload;

    const conditions = daysCheckedArray.map((day) => {
      return {
        startingTime: { [Op.startsWith]: moment(day).format("YYYY-MM-DD") },
      };
    });

    const existedMeeting = await model.findAll({
      where: {
        roomId,
        status: { [Op.ne]: "cancel" },
        [Op.or]: conditions,
      },
      attributes: ["startingTime", "endingTime"],
      raw: true,
    });

    const conflict = {
      meeting: [],
      day: [],
    };
    timeCheckedArray.forEach((day) => {
      const { startingTime: createStarting, endingTime: createEnding } = day;

      existedMeeting.forEach((meeting) => {
        let { startingTime: existStarting, endingTime: existEnding } = meeting;

        existStarting = moment(existStarting).format("YYYY-MM-DD HH:mm:ss");
        existEnding = moment(existEnding).format("YYYY-MM-DD HH:mm:ss");
        existDay = moment(meeting.startingTime).format("YYYY-MM-DD");

        if (
          (existStarting <= createStarting && existEnding > createStarting) ||
          (existStarting < createEnding && existEnding >= createEnding) ||
          (existStarting > createStarting && existEnding < createEnding) ||
          (existStarting === createEnding && existEnding === createEnding)
        ) {
          conflict.meeting.push(meeting);
          conflict.day.findIndex((day) => day === existDay) === -1 &&
            conflict.day.push(existDay);
        }
        // throw new AppError(400, "Khung giờ này hiện đã có lịch đăng ký.");
      });
    });

    return conflict;
  },
};

module.exports = validation;
