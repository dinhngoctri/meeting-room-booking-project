class AppError extends Error {
  constructor(status, message) {
    super(message);
    this.status = status;
  }
}

const handleError = (error, req, res, next) => {
  if (!(error instanceof AppError)) {
    error = new AppError(500, "Internal Server Error");
  }

  const { status, message } = error;
  res.status(status).json({ status: "error", message });

  next();
};

module.exports = { AppError, handleError };
