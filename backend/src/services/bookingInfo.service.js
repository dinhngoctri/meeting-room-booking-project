const _ = require("lodash");
const { AppError } = require("../helpers/error");
const { isExisted } = require("../helpers/validation");
const {
  User,
  Room,
  BookingInfo,
  Participant,
  Department,
  Sequelize,
  sequelize,
} = require("../models/index");
const Op = Sequelize.Op;
const moment = require("moment");

const BookingInfoService = {
  async getBookingList(req) {
    try {
      const { roomId, page = 1, pageSize, status, role, userId, date } = req.query;

      const pageParseInt = parseInt(page);
      const pageSizeParseInt = parseInt(pageSize);
      const offset = (pageParseInt - 1) * pageSizeParseInt;

      if (roomId === null) roomId = "";
      if (status === null) status = "";
      if (date === null) date = "";

      // -------------------- user ----------------------------

      if (role === "admin") {
        const payload = {
          pageSizeParseInt,
          offset,
          roomId,
          status,
          userId,
          date,
        };

        if (!date) {
          return this.getBookingListRoleAdminNotDate(payload);
        } else {
          return this.getBookingListRoleAdminHaveDate(payload);
        }
      } else {
        const payload = {
          pageSizeParseInt,
          offset,
          roomId,
          status,
          userId,
          date,
        };

        return this.getBookingListHaveDateOrNotDate(payload);
      }
    } catch (error) {
      console.log("error: ", 11, error);
      throw error;
    }
  },
  async deletedBookingInfo(id, reason, req) {
    try {
      const { userIdLogin, bookingId, role } = req.body;

      const bookingUserId = await BookingInfo.findOne({
        where: { id: bookingId },
        raw: true,
      });

      if (role === "admin") {
        const payload = { model: BookingInfo, attribute: "id", value: id };
        const checkBookingInfoById = await isExisted(payload);
        if (!checkBookingInfoById)
          throw AppError(404, "không tìm thấy id của booking info");
        const canceled = {
          ...checkBookingInfoById,
          status: "cancel",
          reason,
        };
        return await BookingInfo.update(canceled, { where: { id } });
      }

      if (bookingUserId.userId !== userIdLogin)
        throw new AppError(
          400,
          "bạn không phải là chủ cuộc họp , nên bạn không có quyền xóa cuộc họp"
        );

      const payload = { model: BookingInfo, attribute: "id", value: id };
      const checkBookingInfoById = await isExisted(payload);
      if (!checkBookingInfoById) throw AppError(404, "không tìm thấy id của booking");
      const canceled = {
        ...checkBookingInfoById,
        status: "cancel",
        reason,
      };

      const result = await BookingInfo.update(canceled, { where: { id } });

      return result;
    } catch (error) {
      throw error;
    }
  },

  async saveEmailToParticipants(payload) {
    try {
      let participantList = [];
      const { bookingId, emailList, userIdLogin } = payload.body;

      const userIdArr = emailList
        .filter((item) => typeof item === "number")
        .map((item) => {
          return {
            userId: item,
          };
        });

      const userEmailArr = emailList.filter((item) => typeof item === "string");

      const bookingDetailUnderData = await BookingInfo.findOne({
        where: { id: bookingId },
        raw: true,
      });

      const checkAdmin = await User.findOne({
        where: {
          id: userIdLogin,
        },
      });

      // ----------------- admin ---------------------

      if (checkAdmin.role === "admin") {
        if (!bookingId) throw new AppError(400, "booking id đang undifine");
        if (emailList.length === 0)
          throw new AppError(400, "danh sách email gửi đi đang bị rỗng");

        const findIdOfEmail = await User.findAll({
          where: { email: userEmailArr },
          raw: true,
        }).map((item) => {
          return {
            userId: item.id,
          };
        });

        findIdOfEmail.push(...userIdArr);

        const arrUserParticipantUnderDatabase = await Participant.findAll({
          where: { bookingId },
          raw: true,
        });

        // lấy ra những thằng giống nhau

        const intersectionBy = _.intersectionBy(
          findIdOfEmail,
          arrUserParticipantUnderDatabase,
          "userId"
        );

        let arrUserId = intersectionBy.map((item) => {
          return item.userId;
        });

        if (arrUserId) {
          await Participant.update(
            { deleted: 0 },
            { where: { bookingId, userId: arrUserId } }
          );
        }

        const differenceByUserId = _.differenceBy(
          findIdOfEmail,
          intersectionBy,
          "userId"
        );

        differenceByUserId.map((item) => {
          const data = {
            userId: item.userId,
            bookingId,
            announced: 0,
            deleted: 0,
          };
          participantList.push(data);
        });

        const respone = await Participant.bulkCreate(participantList);
        return respone.map((participant) => participant.toJSON());
      }

      // ----------------- user ---------------------

      if (bookingDetailUnderData.status !== "comingUp")
        throw new AppError(400, "cuộc họp đã xong hoặc đã bị hủy");

      if (bookingDetailUnderData.userId !== userIdLogin) {
        throw new AppError(
          400,
          "bạn không phải là chủ cuộc họp , nên bạn không có quyền gửi email"
        );
      }

      if (!bookingId) throw new AppError(400, "booking id đang có giá trị undifine");
      if (emailList.length === 0)
        throw new AppError(400, "danh sách email gửi không được trống");

      const findIdOfEmail = await User.findAll({
        where: { email: userEmailArr },
        raw: true,
      }).map((item) => {
        return {
          userId: item.id,
        };
      });

      findIdOfEmail.push(...userIdArr);

      const arrUserParticipantUnderDatabase = await Participant.findAll({
        where: { bookingId },
        raw: true,
      });

      // lấy ra những thằng giống nhau

      const intersectionBy = _.intersectionBy(
        findIdOfEmail,
        arrUserParticipantUnderDatabase,
        "userId"
      );

      let arrUserId = intersectionBy.map((item) => {
        return item.userId;
      });

      if (arrUserId) {
        await Participant.update(
          { deleted: 0 },
          { where: { bookingId, userId: arrUserId } }
        );
      }

      const differenceByUserId = _.differenceBy(findIdOfEmail, intersectionBy, "userId");

      differenceByUserId.map((item) => {
        const data = {
          userId: item.userId,
          bookingId,
          announced: 0,
          deleted: 0,
        };
        participantList.push(data);
      });

      const respone = await Participant.bulkCreate(participantList);
      const participants = respone.map((participant) => participant.toJSON());

      return participants;
    } catch (error) {
      throw error;
    }
  },

  async searchBookingInfo(req) {
    const {
      fullname,
      roomId,
      page = 1,
      pageSize,
      status,
      userId,
      role,
      date,
    } = req.query;
    const pageParseInt = parseInt(page);
    const pageSizeParseInt = parseInt(pageSize);
    const offset = (pageParseInt - 1) * pageSizeParseInt;

    if (roomId === null) roomId = "";
    if (status === null) status = "";

    if (role === "admin") {
      const payload = {
        pageSizeParseInt,
        offset,
        roomId,
        status,
        userId,
        fullname,
        date,
      };

      if (!date) {
        return this.getBookingListRoleAdminNotDate(payload);
      } else {
        return this.getBookingListRoleAdminHaveDate(payload);
      }
    }

    if (role === "user") {
      const payload = {
        pageSizeParseInt,
        offset,
        roomId,
        status,
        userId,
        fullname,
        date,
      };

      return this.getBookingListHaveDateOrNotDate(payload);
    }
  },

  async listOfMeetingsYouAttend(req) {
    try {
      const { roomId, page = 1, pageSize, status, role, userId, date } = req.query;

      const pageParseInt = parseInt(page);
      const pageSizeParseInt = parseInt(pageSize);
      const offset = (pageParseInt - 1) * pageSizeParseInt;

      if (roomId === null) roomId = "";
      if (status === null) status = "";

      if (role === "admin") {
        const payload = {
          pageSizeParseInt,
          offset,
          roomId,
          status,
          userId,
          date,
        };

        if (!date) {
          return this.getBookingListNotDate(payload);
        } else {
          return this.getBookingListHaveDate(payload);
        }
      }
    } catch (error) {
      throw error;
    }
  },

  async searchBookingInfoForAdmin(req) {
    try {
      const {
        fullname,
        roomId,
        page = 1,
        pageSize,
        status,
        userId,
        role,
        date,
      } = req.query;
      const pageParseInt = parseInt(page);
      const pageSizeParseInt = parseInt(pageSize);
      const offset = (pageParseInt - 1) * pageSizeParseInt;

      if (roomId === null) roomId = "";
      if (status === null) status = "";

      if (role === "admin") {
        const payload = {
          pageSizeParseInt,
          offset,
          roomId,
          status,
          userId,
          fullname,
          date,
        };

        if (!date) {
          return this.getBookingListNotDate(payload);
        } else {
          return this.getBookingListHaveDate(payload);
        }
      }
    } catch (error) {
      throw error;
    }
  },

  async addGuestAccount(req) {
    try {
      const { guestEmail } = req.body;

      const guestExist = await User.findAll({
        where: { email: guestEmail },
        attribute: ["id", "email"],
        raw: true,
      });

      const emailExist = guestExist.map((guest) => guest.email);

      const newGuest = [];
      guestEmail.forEach((email) => {
        if (!emailExist.includes(email)) {
          newGuest.push({
            fullname: email.split("@gmail.com")[0],
            email,
            departmentId: -1,
            refId: -1,
          });
        }
      });

      const newUser = await User.bulkCreate(newGuest);

      return [...newUser, ...guestExist];
    } catch (error) {
      throw error;
    }
  },

  async getDateList(req) {
    const { userId, status, roomId, fullname = "", role, active } = req.body;

    let bookingIdList = null;

    if (role === "user") {
      bookingIdList = await Participant.findAll({
        attributes: ["bookingId"],
        where: {
          userId,
          deleted: 0,
        },
        raw: true,
      });
    }

    if (role === "admin") {
      if (!active) {
        bookingIdList = await Participant.findAll({
          attributes: ["bookingId"],
          where: {
            deleted: 0,
          },
          raw: true,
        });
      } else {
        bookingIdList = await Participant.findAll({
          attributes: ["bookingId"],
          where: {
            userId,
            deleted: 0,
          },
          raw: true,
        });
      }
    }

    bookingIdList = bookingIdList.map((item) => item.bookingId);

    let result = await BookingInfo.findAll({
      order: [["startingTime", "ASC"]],
      attributes: ["startingTime"],
      include: [
        {
          model: User,
          as: "user",
          attributes: ["email", "fullname"],
          where: {
            fullname: { [Sequelize.Op.like]: `%${fullname}%` },
          },
        },
      ],

      where: {
        id: bookingIdList,
        status,
        roomId,
      },
      raw: true,
    });

    result = result.map((item) => {
      return {
        value: item.startingTime,
        text: moment(item.startingTime).format("DD/MM/YYYY"),
      };
    });

    result.unshift({ value: null, text: "Xem Theo Ngày" });

    const filteredData = result.reduce((accumulator, currentValue) => {
      const existingObject = accumulator.find((obj) => obj.text === currentValue.text);
      if (!existingObject) {
        accumulator.push(currentValue);
      }
      return accumulator;
    }, []);

    return filteredData;
  },

  async getBookingListHaveDateOrNotDate(payload) {
    const { date } = payload;
    if (!date) {
      return this.getBookingListNotDate(payload);
    } else {
      return this.getBookingListHaveDate(payload);
    }
  },

  async getBookingListNotDate(payload) {
    const { pageSizeParseInt, offset, roomId, status, userId, fullname = "" } = payload;
    const { count, rows } = await Participant.findAndCountAll({
      order: [["id", "ASC"]],
      offset,
      limit: pageSizeParseInt,
      include: [
        {
          model: BookingInfo,
          as: "booking",
          include: [
            {
              model: Department,
              as: "department",
              attributes: ["departmentName"],
            },
            {
              model: Room,
              as: "room",
              attributes: ["roomName", "seats"],
            },
            {
              model: User,
              as: "user",
              attributes: ["email", "fullname"],
              where: {
                fullname: { [Sequelize.Op.like]: `%${fullname}%` },
              },
            },
          ],
          where: { roomId, status },
        },
        {
          model: User,
          as: "user",
          attributes: ["email", "id", "fullname"],
        },
      ],
      where: { userId, deleted: 0 },
      raw: true,
    });

    return {
      count,
      data: rows,
    };
  },

  async getBookingListHaveDate(payload) {
    const {
      pageSizeParseInt,
      offset,
      roomId,
      status,
      userId,
      date,
      fullname = "",
    } = payload;

    const dateStringBeforeT = date.slice(0, date.indexOf("T"));
    const targetDate = new Date(dateStringBeforeT);

    const { count, rows } = await Participant.findAndCountAll({
      order: [["id", "ASC"]],
      offset,
      limit: pageSizeParseInt,
      include: [
        {
          model: BookingInfo,
          as: "booking",
          include: [
            {
              model: Department,
              as: "department",
              attributes: ["departmentName"],
            },
            {
              model: Room,
              as: "room",
              attributes: ["roomName", "seats"],
            },
            {
              model: User,
              as: "user",
              attributes: ["email", "fullname"],
              where: {
                fullname: { [Sequelize.Op.like]: `%${fullname}%` },
              },
            },
          ],
          where: {
            roomId,
            status,
            startingTime: {
              [Op.between]: [
                targetDate.setHours(0, 0, 0, 0),
                targetDate.setHours(23, 59, 59, 999),
              ],
            },
          },
        },
        {
          model: User,
          as: "user",
          attributes: ["email", "id", "fullname"],
        },
      ],
      where: { userId, deleted: 0 },
      raw: true,
    });
    return {
      count,
      data: rows,
    };
  },

  async getBookingListRoleAdminNotDate(payload) {
    const { pageSizeParseInt, offset, roomId, status, fullname = "" } = payload;
    const { count, rows } = await BookingInfo.findAndCountAll({
      order: [["startingTime", "ASC"]],
      offset,
      limit: pageSizeParseInt,
      include: [
        {
          model: User,
          as: "user",
          attributes: ["fullname"],
          where: {
            fullname: { [Sequelize.Op.like]: `%${fullname}%` },
          },
        },
        {
          model: Department,
          as: "department",
          attributes: ["departmentName"],
        },
        {
          model: Room,
          as: "room",
          attributes: ["seats", "roomName"],
        },
      ],
      where: { roomId, status },
      raw: true,
    });
    return {
      count,
      data: rows,
    };
  },

  async getBookingListRoleAdminHaveDate(payload) {
    const { pageSizeParseInt, offset, roomId, status, fullname = "", date } = payload;

    const dateStringBeforeT = date.slice(0, date.indexOf("T"));
    const targetDate = new Date(dateStringBeforeT);

    const { count, rows } = await BookingInfo.findAndCountAll({
      order: [["startingTime", "ASC"]],
      offset,
      limit: pageSizeParseInt,
      include: [
        {
          model: User,
          as: "user",
          attributes: ["fullname"],
          where: {
            fullname: { [Sequelize.Op.like]: `%${fullname}%` },
          },
        },
        {
          model: Department,
          as: "department",
          attributes: ["departmentName"],
        },
        {
          model: Room,
          as: "room",
          attributes: ["seats", "roomName"],
        },
      ],
      where: {
        roomId,
        status,
        startingTime: {
          [Op.between]: [
            targetDate.setHours(0, 0, 0, 0),
            targetDate.setHours(23, 59, 59, 999),
          ],
        },
      },
      raw: true,
    });
    return {
      count,
      data: rows,
    };
  },
};

module.exports = BookingInfoService;
