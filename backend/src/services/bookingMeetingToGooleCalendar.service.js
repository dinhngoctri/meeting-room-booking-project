const { google } = require("googleapis");
// const key = require("../json/key.json");
const { AppError } = require("../helpers/error");
const _ = require("lodash");
const { BookingInfo, User, Sequelize, sequelize } = require("../models/index");
const moment = require("moment");
const momentTimezone = require("moment-timezone");
const Op = Sequelize.Op;
require("dotenv").config();

const jwtClient = new google.auth.JWT(
  process.env.CLIENT_EMAIL,
  null,
  process.env.PRIVATE_KEY,
  [
    "https://www.googleapis.com/auth/calendar",
    "https://www.googleapis.com/auth/calendar.readonly",
    "https://www.googleapis.com/auth/calendar.events.readonly",
    "https://www.googleapis.com/auth/calendar.events",
  ],
  "noreply_ddv_system@didongviet.vn"
);
const calendar = google.calendar({ version: "v3", auth: jwtClient });
const CALENDAR_ID = process.env.CALENDAR_ID;
// "ddv_meeting@didongviet.vn";

const bookingMeetingToGoogleCalendar = {
  async findEventId(eventId) {
    try {
      const result = await calendar.events.get({
        auth: jwtClient,
        calendarId: CALENDAR_ID,
        eventId,
        singleEvents: true,
      });

      return result.data;
    } catch (error) {
      throw error;
    }
  },

  async getEmailFromGuestIdlList(guestIdlList) {
    return await User.findAll({
      where: { id: guestIdlList },
      attributes: ["email"],
      raw: true,
    }).filter((item) => item.email !== "");
  },

  async createEventToGoogleCalendar(req) {
    try {
      const {
        meetingContent, //sumary
        location = "79 Đồng Khởi",
        description = "",
        startTime,
        endTime,
        userArray, //5,29,30
        recurringMeeting,
        bookingId,
      } = req.body;

      console.log("createEventToGoogleCalendar-reqbody:", req.body);

      let guestEmailList = [];
      if (userArray.length > 0) {
        guestEmailList = await this.getEmailFromGuestIdlList(userArray);
      }

      if (recurringMeeting === "accepted") {
        let periodic = [];

        const byday = moment(startTime).locale("en").format("dd").toUpperCase();
        const lastDayOfMonth = moment(startTime).endOf("month");
        const until = momentTimezone(lastDayOfMonth)
          .utc()
          .format("YYYYMMDD[T]HHmmss[Z]");
        periodic = [`RRULE:FREQ=MONTHLY;BYDAY=${byday};UNTIL=${until}`];

        const event = {
          summary: meetingContent,
          location,
          description,
          start: {
            dateTime: startTime,
            timeZone: "Asia/Ho_Chi_Minh",
          },
          end: {
            dateTime: endTime,
            timeZone: "Asia/Ho_Chi_Minh",
          },
          sendNotifications: true,
          recurrence: periodic,
          attendees: guestEmailList,
          reminders: {
            useDefault: true,
          },
        };

        const response = await calendar.events.insert({
          auth: jwtClient,
          calendarId: CALENDAR_ID,
          resource: event,
        });

        const eventId = response.data.id;

        const recurringMeetingIdList = await calendar.events.instances({
          calendarId: process.env.CALENDAR_ID,
          eventId,
          timeMin: new Date(startTime).toISOString(),
          maxResults: 5,
        });

        bookingId.forEach(async (item, i) => {
          await BookingInfo.update(
            { eventId: recurringMeetingIdList.data.items[i].id },
            {
              where: { id: item },
            }
          );
        });
        return recurringMeetingIdList.data.items;
      }

      const event = {
        summary: meetingContent,
        location,
        description,
        start: {
          dateTime: startTime,
          timeZone: "Asia/Ho_Chi_Minh",
        },
        end: {
          dateTime: endTime,
          timeZone: "Asia/Ho_Chi_Minh",
        },
        sendNotifications: true,
        attendees: guestEmailList,
        reminders: {
          useDefault: true,
        },
      };

      const response = await calendar.events.insert({
        auth: jwtClient,
        calendarId: CALENDAR_ID,
        resource: event,
      });

      const eventId = response.data.id;

      console.log("eventId", eventId);

      const insertEventId = await BookingInfo.update(
        { eventId },
        {
          where: { id: bookingId },
        }
      );

      return { response, insertEventId };
    } catch (error) {
      throw error;
    }
  },

  async deletedEventToGoogleCalendar(req, next) {
    try {
      const bookingId = req.params.id;

      console.log("bookingId", bookingId);

      if (!bookingId) throw new AppError(400, "không tìm thấy bookingId");

      const { eventId } = await BookingInfo.findOne({
        where: { id: bookingId },
        attributes: ["eventId"],
        raw: true,
      });

      if (!eventId) throw new AppError(404, "không tìm thâý eventId");

      await calendar.events.delete(
        {
          auth: jwtClient,
          calendarId: CALENDAR_ID,
          eventId,
          sendNotifications: true,
        },
        function (err) {
          if (err) {
            return err;
          }
          console.log("Đã xóa sự kiện thành công.");
          return {
            msg: "Đã xóa sự kiện thành công",
            eventId,
          };
        }
      );
    } catch (error) {
      next(error);
      throw error;
    }
  },

  async inviteMoreGuest(req) {
    try {
      const { userArray, eventId } = req.body;
      let guestEmailList = [];
      if (userArray.length > 0) {
        guestEmailList = await this.getEmailFromGuestIdlList(userArray);
      } else {
        throw new AppError(400, "không có người dùng để mời vào sự kiện");
      }

      if (!eventId) throw new AppError(404, "không tìm thâý eventId");

      console.log("guestEmailList", guestEmailList);

      const { attendees } = await this.findEventId(eventId);

      console.log("attendees", attendees);

      let newAttendees = [];

      attendees
        ? (newAttendees = [...attendees, ...guestEmailList])
        : (newAttendees = guestEmailList);

      await calendar.events.patch(
        {
          auth: jwtClient,
          calendarId: CALENDAR_ID,
          eventId,
          resource: {
            attendees: newAttendees,
          },
        },
        function (err, res) {
          if (err) {
            throw new AppError(400, "Lỗi khi cập nhật sự kiện: " + err);
          }
          console.log("Người dùng đã được mời thành công vào sự kiện.");
          return {
            msg: "Người dùng đã được mời thành công vào sự kiện.",
          };
        }
      );
    } catch (error) {
      throw error;
    }
  },

  async deleteGuestOutOfMeeting(req, next) {
    try {
      const { userId, bookingId } = req.query;

      const emailRemove = await User.findOne({
        where: { id: userId },
        attributes: ["email"],
        raw: true,
      });

      const { eventId } = await BookingInfo.findOne({
        where: { id: bookingId },
        attributes: ["eventId"],
        raw: true,
      });

      if (!eventId) throw new AppError(404, "không tìm thâý eventId");

      const { attendees } = await this.findEventId(eventId);

      // Tìm index của người dùng cần xóa trong danh sách attendees
      const userIndex = attendees.findIndex(
        (attendee) => attendee.email === emailRemove.email
      );

      console.log("userIndex", userIndex);
      // Nếu người dùng không tồn tại trong danh sách attendees, thông báo lỗi
      if (userIndex === -1) {
        throw new AppError(
          400,
          "Người dùng không tồn tại trong danh sách tham dự sự kiện."
        );
      }

      // Tạo đối tượng thay đổi để xóa người dùng khỏi danh sách tham dự
      const change = {
        eventId,
        calendarId: CALENDAR_ID,
        auth: jwtClient,
        sendNotifications: true,
        resource: {
          attendees: attendees.filter((_, i) => i !== userIndex),
        },
      };

      // Sử dụng method events.patch để cập nhật sự kiện
      await calendar.events.patch(change, function (err, res) {
        if (err) {
          throw new AppError(
            400,
            "Lỗi khi xóa người dùng khỏi danh sách tham dự sự kiện: " + err
          );
        }
        return res;
      });
    } catch (error) {
      throw error;
    }
  },
};

module.exports = bookingMeetingToGoogleCalendar;
