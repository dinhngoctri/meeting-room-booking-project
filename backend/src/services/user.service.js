const { AppError } = require("../helpers/error");
const { User, Department, Sequelize } = require("../models/index");
const { isEmpty, isExisted, isEmail } = require("../helpers/validation");
const bcrypt = require("bcrypt");
const { generateToken } = require("../helpers/jwt");
const _ = require("lodash");
const { Op } = require("sequelize");

const UserService = {
  async login(userInput) {
    const { email, password } = userInput;
    try {
      const payload = { model: User, attribute: "email", value: email };
      const user = await isExisted(payload);

      if (!user) {
        throw new AppError(400, "Email hoặc mật khẩu không chính xác.");
      }

      const isMatched = bcrypt.compareSync(password, user.password);
      if (!isMatched) {
        throw new AppError(400, "Email hoặc mật khẩu không chính xác.");
      }
      return generateToken({
        id: user.id,
        email: user.email,
        departmentId: user.departmentId,
        role: user.role,
      });
    } catch (error) {
      throw error;
    }
  },

  async register(userInput) {
    const { email, password, departmentId } = userInput;

    try {
      if (isEmpty(email)) {
        throw new AppError(400, "Email không được bỏ trống.");
      }
      if (isEmpty(password)) {
        throw new AppError(400, "Mật khẩu không được bỏ trống.");
      }
      if (!departmentId) {
        throw new AppError(400, "Phòng ban không được bỏ trống.");
      }

      const payload = { model: User, attribute: "email", value: email };
      const isEmailExisted = await isExisted(payload);
      if (!isEmail(email)) {
        throw new AppError(400, "Email không đúng định dạng.");
      }
      if (isEmailExisted) {
        throw new AppError(
          400,
          "Email này đã được dùng để đăng ký cho một tài khoản khác."
        );
      }

      if (password.length < 4 || password.length > 30) {
        throw new AppError(
          400,
          "Độ dài mật khẩu phải nằm trong khoảng từ 4 đến 30 ký tự."
        );
      }

      await User.create(userInput);
    } catch (error) {
      throw error;
    }
  },

  async findUserByRefId(refId) {
    try {
      const user = await User.findOne({ where: { refId } });
      return user || null;
    } catch (error) {
      throw error;
    }
  },

  async accessByDCore(body) {
    try {
      const { refId } = body;
      let user = await User.findOne({ where: { refId } });

      if (!user) {
        user = await User.create(body);
      } else {
        await User.update(body, { where: { id: user.id } });
        user = await User.findByPk(user.id);
      }

      return generateToken({
        id: user.id,
        email: user.email,
        departmentId: user.departmentId,
        role: user.role,
        fullname: user.fullname,
      });
    } catch (error) {
      throw error;
    }
  },

  // todo lấy danh sách tất cả user
  async takeAListOfUser(req) {
    try {
      const { departmentId, page = 1, pageSize } = req.query;

      const pageParseInt = parseInt(page);
      const pageSizeParseInt = parseInt(pageSize);

      const offset = (pageParseInt - 1) * pageSizeParseInt;

      if (!departmentId) {
        const { count, rows } = await User.findAndCountAll({
          where: {
            deleted: false,
          },
          order: [["id", "ASC"]],
          offset,
          limit: pageSizeParseInt,
          include: [
            {
              model: Department,
              as: "department",
              attributes: ["id", "departmentName"],
              raw: true,
            },
          ],
        });

        // const totalPages = Math.ceil(count / pageSizeParseInt);

        return {
          count,
          data: rows,
        };
      }
      const { count, rows } = await User.findAndCountAll({
        where: {
          departmentId,
          deleted: false,
        },

        order: [["id", "ASC"]],
        offset,
        limit: pageSizeParseInt,
        include: [
          {
            model: Department,
            as: "department",
            attributes: ["id", "departmentName"],
            raw: true,
          },
        ],
      });

      // const totalPages = Math.ceil(count / pageSizeParseInt);

      return {
        count,
        data: rows,
      };
    } catch (error) {
      throw error;
    }
  },

  async takeAListOfUserDelelte(req) {
    try {
      const { departmentId, page = 1, pageSize } = req.query;

      const pageParseInt = parseInt(page);
      const pageSizeParseInt = parseInt(pageSize);

      const offset = (pageParseInt - 1) * pageSizeParseInt;

      if (!departmentId) {
        const { count, rows } = await User.findAndCountAll({
          where: {
            deleted: true,
          },
          order: [["id", "ASC"]],
          offset,
          limit: pageSizeParseInt,
          include: [
            {
              model: Department,
              as: "department",
              attributes: ["id", "departmentName"],
              raw: true,
            },
          ],
        });
        return {
          count,
          data: rows,
        };
      }
      const { count, rows } = await User.findAndCountAll({
        where: {
          departmentId,
          deleted: true,
        },

        order: [["id", "ASC"]],
        offset,
        limit: pageSizeParseInt,
        include: [
          {
            model: Department,
            as: "department",
            attributes: ["id", "departmentName"],
            raw: true,
          },
        ],
      });
      return {
        count,
        data: rows,
      };
    } catch (error) {
      throw error;
    }
  },

  // todo cập nhật user
  async updateUser(id, req) {
    try {
      if (id === "") throw AppError(404, "id not found");
      if (req.body === {}) throw AppError(400, "missing data field");
      const { email, role, departmentId } = req.body;
      const payload = { model: User, attribute: "id", value: id };
      const checkUserID = await isExisted(payload);
      if (checkUserID) {
        const update = {
          email,
          role,
          departmentId,
        };
        const result = await User.update(update, { where: { id } });
        return result;
      }
      throw AppError(404, "checkUserID not found");
    } catch (error) {
      throw error;
    }
  },

  // todo xóa user
  async deletedUser(id) {
    try {
      if (id === "") throw AppError(404, "id not found");
      const payload = { model: User, attribute: "id", value: id };
      const checkUserID = await isExisted(payload);
      if (checkUserID) {
        const update = {
          deleted: true,
        };
        const result = await User.update(update, { where: { id } });
        return result;
      }
      throw AppError(404, "checkUserID not found");
    } catch (error) {
      throw error;
    }
  },

  // todo khôi phục lại tài khoản
  async restoreUser(id) {
    try {
      if (id === "") throw AppError(404, "id not found");

      const payload = { model: User, attribute: "id", value: id };
      const checkUserID = await isExisted(payload);

      if (checkUserID) {
        const update = {
          deleted: false,
        };
        const result = await User.update(update, { where: { id } });
        return result;
      }
      return false;
    } catch (error) {
      throw error;
    }
  },

  async changePassword(id, currentPassword, newPassword, confirmPassword) {
    try {
      const payload = { model: User, attribute: "id", value: id };
      const isUserdExisted = await isExisted(payload);
      if (!isUserdExisted) throw AppError(404, "Not Found By ID");

      const isMatched = bcrypt.compareSync(currentPassword, isUserdExisted.password);

      if (newPassword !== confirmPassword)
        throw AppError(400, "Password is not the same confirmPassword");

      const newPass = { ...isUserdExisted, password: newPassword };

      if (!isMatched) throw AppError(400, "password is not correct");

      return await User.update(newPass, { where: { id } });
    } catch (error) {
      throw error;
    }
  },

  // todo lấy danh sách user theo phòng ban

  async getUserListByDepartment(req) {
    try {
      const { arrUserInMeeting, userIdLogin } = req.body;

      if (arrUserInMeeting && arrUserInMeeting.length > 0) {
        const newArrUserInMeeting = arrUserInMeeting.map((item) => item.id);
        let userPerDepartment = await Department.findAll({
          include: [
            {
              model: User,
              as: "Users",
              raw: true,
              where: {
                id: {
                  [Op.notIn]: newArrUserInMeeting,
                },
              },
            },
          ],
        });
        return userPerDepartment;
      } else if (userIdLogin) {
        let userPerDepartment = await Department.findAll({
          include: [
            {
              model: User,
              as: "Users",
              raw: true,
              where: {
                id: {
                  [Op.not]: userIdLogin,
                },
              },
            },
          ],
        });
        return userPerDepartment;
      }
    } catch (error) {
      throw error;
    }
  },

  async searchUser(req) {
    try {
      const { email, id, departmentId, page = 1, pageSize, deleted } = req.query;

      const z1 = /^[0-9]*$/;

      let searchById = null;
      let seachByEmail = null;
      const pageParseInt = parseInt(page);
      const pageSizeParseInt = parseInt(pageSize);

      const offset = (pageParseInt - 1) * pageSizeParseInt;

      if (z1.test(id)) {
        searchById = id;
      } else {
        seachByEmail = email;
      }

      if (!departmentId) {
        const { count, rows } = await User.findAndCountAll({
          where: {
            deleted,
            [Sequelize.Op.or]: [
              { email: { [Sequelize.Op.like]: `%${seachByEmail}%` } },
              { id: { [Sequelize.Op.like]: `%${searchById}%` } },
            ],
          },
          order: [["id", "ASC"]],
          offset,
          limit: pageSizeParseInt,
          include: [
            {
              model: Department,
              as: "department",
              attributes: ["id", "departmentName"],
              raw: true,
            },
          ],
        });
        return {
          count,
          data: rows,
        };
      }

      const { count, rows } = await User.findAndCountAll({
        where: {
          departmentId,
          deleted,
          [Sequelize.Op.or]: [
            { email: { [Sequelize.Op.like]: `%${seachByEmail}%` } },
            { id: { [Sequelize.Op.like]: `%${searchById}%` } },
          ],
        },

        order: [["id", "ASC"]],
        offset,
        limit: pageSizeParseInt,
        include: [
          {
            model: Department,
            as: "department",
            attributes: ["id", "departmentName"],
            raw: true,
          },
        ],
      });

      return {
        count,
        data: rows,
      };

      // return await User.findAll({
      //   where: { departmentId },
      //   include: [
      //     {
      //       model: Department,
      //       as: "department",
      //       attributes: ["id", "departmentName"],
      //       raw: true,
      //     },
      //   ],
      //   where: {
      //     [Sequelize.Op.or]: [
      //       { email: { [Sequelize.Op.like]: `%${seachByEmail}%` } },
      //       { id: { [Sequelize.Op.like]: `%${searchById}%` } },
      //     ],
      //   },
      // });
    } catch (error) {
      throw error;
    }
  },

  async addUserInList(req) {
    try {
      const { email, role, departmentId, password } = req.body;
      const newUser = {
        email,
        role,
        departmentId,
        password,
      };
      return await User.create(newUser);
    } catch (error) {
      throw error;
    }
  },

  async deleteAlotOfUser(arrUserId) {
    try {
      return await User.update({ deleted: 1 }, { where: { id: arrUserId } });
    } catch (error) {
      throw error;
    }
  },
};

module.exports = UserService;
