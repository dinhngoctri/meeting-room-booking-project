const { Op } = require("sequelize");
const { AppError } = require("../helpers/error");
const { BookingInfo, User, Department, Room, Participant } = require("../models/index");
const {
  isExisted,
  isEmpty,
  isNumber,
  isValidTimeFrame,
} = require("../helpers/validation");
const moment = require("moment");

const MeetingService = {
  async getListOfMeeting() {
    try {
      return await BookingInfo.findAll();
    } catch (error) {
      throw error;
    }
  },

  async getBookingInfoByRoomID(roomId) {
    try {
      const bookingList = await BookingInfo.findAll({ where: { roomId } });
      return bookingList;
    } catch (error) {
      throw error;
    }
  },

  async getTodayMeetings(today) {
    try {
      return BookingInfo.findAll({
        where: {
          status: {
            [Op.ne]: "cancel",
          },
          startingTime: {
            [Op.between]: today,
          },
        },
        attributes: ["id"],
        raw: true,
      });
    } catch (error) {
      throw error;
    }
  },

  async getMeetingInfoForTeleNoti(bookingId) {
    try {
      const participants = await this.getUserListInParticipant(bookingId);
      const meetingInfo = await BookingInfo.findOne({
        where: { id: bookingId },
        include: [
          {
            model: Room,
            as: "room",
            attributes: ["roomName"],
            require: true,
            raw: true,
          },
          {
            model: Department,
            as: "department",
            attributes: ["telegramGroupId"],
            require: true,
            raw: true,
          },
        ],
        attributes: ["startingTime", "endingTime", "meetingContent", "reason"],
        raw: true,
      });

      const { startingTime, endingTime, meetingContent, reason } = meetingInfo;

      return {
        participants,
        startingTime,
        endingTime,
        meetingContent,
        roomName: meetingInfo["room.roomName"],
        chat_id: meetingInfo["department.telegramGroupId"],
        reason,
      };
    } catch (error) {
      throw error;
    }
  },

  async getBookingInfoByRoomIDAndDate(input) {
    const { roomId, week } = input;
    try {
      if (week.length !== 2) {
        throw new AppError(400, "Lỗi định dạng 001");
      }
      console.log(1995);
      const bookingList = await BookingInfo.findAll({
        where: {
          roomId,
          status: {
            [Op.ne]: "cancel",
          },
          startingTime: {
            [Op.between]: week,
          },
        },
        raw: true,
      });

      const formatBookingList = [];
      await Promise.all(
        bookingList.map(async (booking) => {
          const user = await User.findOne({
            where: { id: booking.userId },
            attributes: ["email"],
            raw: true,
          });
          const department = await Department.findOne({
            where: { id: booking.departmentId },
            attributes: ["departmentName", "departmentColor"],
            raw: true,
          });
          const room = await Room.findOne({
            where: { id: booking.roomId },
            attributes: ["roomName"],
            raw: true,
          });
          const data = {
            ...booking,
            email: user ? user.email : "Người dùng hiện không tồn tại.",
            departmentName: department.departmentName,
            departmentColor: department.departmentColor,
            roomName: room.roomName,
          };
          formatBookingList.push(data);
        })
      );

      return formatBookingList;
    } catch (error) {
      console.log("error: ", 1998, error);
      throw error;
    }
  },

  async getDetailMeeting(id) {
    try {
      return BookingInfo.findOne({
        where: { id },
        include: [
          {
            model: User,
            as: "user",
            attributes: ["email", "id", "fullname"],
          },
          {
            model: Department,
            as: "department",
            attributes: ["departmentName"],
          },
          {
            model: Room,
            as: "room",
            attributes: ["seats", "roomName"],
          },
        ],
      });
    } catch (error) {
      throw error;
    }
  },

  async bookingRoom(bookingInfo) {
    try {
      const { userID, departmentId, roomId, startingTime, endingTime } = bookingInfo;

      const now = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");
      if (now > startingTime) {
        throw new AppError(400, "Giờ bắt đầu họp không được sớm hơn thời gian hiện tại");
      }

      if (!isNumber(roomId)) {
        throw new AppError(400, "Id phòng họp phải là kiểu dữ liệu số.");
      }
      if (isEmpty(startingTime)) {
        throw new AppError(400, "Thiếu ngày giờ bắt đầu cuộc họp.");
      }
      if (isEmpty(endingTime)) {
        throw new AppError(400, "Thiếu ngày giờ kết thúc cuộc họp.");
      }

      const dateCompare = startingTime <= endingTime;
      if (!dateCompare) {
        throw new AppError(
          400,
          "Thời gian kết thúc không được bằng hoặc sớm hơn thời gian bắt đầu cuộc họp."
        );
      }

      if (!isExisted({ model: User, attribute: "id", value: userID })) {
        throw new AppError(400, "Id người dùng không hợp lệ.");
      }
      if (!isExisted({ model: Department, attribute: "id", value: departmentId })) {
        throw new AppError(400, "Vui lòng chọn phòng ban.");
      }
      if (!isExisted({ model: Room, attribute: "id", value: roomId })) {
        throw new AppError(400, "Id phòng họp không hợp lệ.");
      }

      // const invalidTimeFrame = await BookingInfo.findAll({
      //   where: {
      //     roomId,
      //     status: {
      //       [Op.ne]: "cancel",
      //     },
      //     [Op.or]: [
      //       {
      //         [Op.and]: [
      //           { startingTime: { [Op.lte]: startingTime } }, // existStarting <= createStarting
      //           { endingTime: { [Op.gt]: startingTime } }, // existEnding > createStarting
      //         ],
      //       },
      //       {
      //         [Op.and]: [
      //           { startingTime: { [Op.lt]: endingTime } }, // existStarting < createEnding
      //           { endingTime: { [Op.gte]: endingTime } }, // existEnding >= createEnding
      //         ],
      //       },
      //       {
      //         [Op.and]: [
      //           { startingTime: { [Op.gt]: startingTime } }, // existStarting > createStarting
      //           { endingTime: { [Op.lt]: endingTime } }, // existEnding < createEnding
      //         ],
      //       },
      //       {
      //         [Op.and]: [
      //           { startingTime }, // existStarting = createEnding
      //           { endingTime }, // existEnding = createEnding
      //         ],
      //       },
      //     ],
      //   },
      //   raw: true,
      // });

      // const startingDay = moment(startingTime).format("YYYY-MM-DD");
      // const bookings = await BookingInfo.findAll({
      //   where: {
      //     roomId,
      //     status: { [Op.ne]: "cancel" },
      //     startingTime: {
      //       [Op.startsWith]: `${startingDay}`,
      //     },
      //   },
      //   attributes: ["startingTime", "endingTime"],
      //   raw: true,
      // });
      // console.log("bookings: ", 2001, bookings);
      // bookings.forEach((booking) => {
      //   let { startingTime: existStarting, endingTime: existEnding } = booking;

      //   existStarting = moment(existStarting).format("YYYY-MM-DD HH:mm:ss");
      //   existEnding = moment(existEnding).format("YYYY-MM-DD HH:mm:ss");

      //   if (existStarting <= startingTime && existEnding > startingTime)
      //     throw new AppError(400, "Khung giờ này hiện đã có lịch đăng ký.");
      //   if (existStarting < endingTime && existEnding >= endingTime)
      //     throw new AppError(400, "Khung giờ này hiện đã có lịch đăng ký.");
      //   if (existStarting > startingTime && existEnding < endingTime)
      //     throw new AppError(400, "Khung giờ này hiện đã có lịch đăng ký.");
      //   if (existStarting === endingTime && existEnding === endingTime)
      //     throw new AppError(400, "Khung giờ này hiện đã có lịch đăng ký.");
      // });

      // const invalidTimeFrame = await isValidTimeFrame({
      //   model: BookingInfo,
      //   roomId,
      //   startingTime,
      //   endingTime,
      // });

      const conflict = await isValidTimeFrame({
        model: BookingInfo,
        roomId,
        timeCheckedArray: [{ startingTime, endingTime }],
        daysCheckedArray: [startingTime],
      });

      if (conflict.meeting.length) {
        throw new AppError(400, "Khung giờ này hiện đã có lịch đăng ký.");
      }

      return await BookingInfo.create(bookingInfo);
    } catch (error) {
      console.log("error: ", 2000, error);
      throw error;
    }
  },

  async recurringMeetingValidation(payload) {
    const { roomId, recurringArray } = payload;
    const daysCheckedArray = recurringArray.map((day) => day.startingTime);
    try {
      return await isValidTimeFrame({
        model: BookingInfo,
        roomId,
        timeCheckedArray: recurringArray,
        daysCheckedArray,
      });
    } catch (error) {
      console.log("error: ", 99, error);
      throw error;
    }
  },

  async addParticipantsToMeeting(payload) {
    const { meetingArray, userArray } = payload;
    console.log("userArray: ", 22, userArray);

    try {
      const participants = [];

      for (const meeting of meetingArray) {
        userArray.forEach((userId) => {
          participants.push({ userId, bookingId: meeting });
        });
      }

      await Participant.destroy({
        where: { bookingId: meetingArray, userId: userArray },
      });
      await Participant.bulkCreate(participants);
    } catch (error) {
      console.log("error: ", 9090, error);
      throw error;
    }
  },

  async createRecurringMeeting(payload) {
    try {
      const { userId, roomId, recurringArray } = payload;

      const timeCheckedArray = recurringArray.map((meeting) => {
        const { startingTime, endingTime } = meeting;
        return { startingTime, endingTime };
      });
      const daysCheckedArray = recurringArray.map((meeting) => {
        const { startingTime } = meeting;
        return startingTime;
      });

      const conflict = await isValidTimeFrame({
        model: BookingInfo,
        roomId,
        timeCheckedArray,
        daysCheckedArray,
      });

      if (conflict.meeting.length) {
        throw new AppError(
          400,
          "Một hoặc một số khung giờ trống hiện đã có lịch họp. Vui lòng kiểm tra khung giờ trống và đăng ký lại lịch họp định kỳ."
        );
      }

      const meetingArray = recurringArray.map((meeting) => {
        return { ...meeting, userId };
      });
      if (!meetingArray.length)
        throw new AppError(400, "Không tìm thấy khung giờ trống để đặt lịch.");

      const result = await BookingInfo.bulkCreate(meetingArray);
      return result;
    } catch (error) {
      console.log("error: ", 992000, error);
      throw error;
    }
  },

  async updateBookingStatus() {
    try {
      await BookingInfo.update(
        { status: "done" },
        {
          where: {
            status: "comingUp",
            endingTime: { [Op.lt]: new Date() },
          },
          raw: true,
        }
      );
    } catch (error) {
      throw error;
    }
  },

  async cancelBooking(payload) {
    try {
      const { id, reason, userID, role } = payload;
      const booking = await BookingInfo.findByPk(id);

      if (!booking) {
        throw new AppError(404, "Lịch đăng ký không tồn tại.");
      }

      if (booking.status === "done") {
        throw new AppError(400, "Không thể huỷ lịch họp đã kết thúc.");
      }

      if (booking.status === "cancel") {
        throw new AppError(400, "Lịch họp này đã bị huỷ.");
      }

      if (isEmpty(reason)) {
        throw new AppError(400, "Vui lòng cho biết lý do huỷ lịch họp này.");
      }

      if (booking.userID === userID || role === "admin") {
        await BookingInfo.update({ status: "cancel", reason }, { where: { id } });
        return;
      }

      throw new AppError(401, "Bạn không đủ quyền để huỷ lịch họp này.");
    } catch (error) {
      throw error;
    }
  },

  async getUserListInParticipant(id) {
    try {
      const res = await Participant.findAll({
        where: { bookingId: id, deleted: 0 },
        include: [
          {
            model: User,
            as: "user",
            attributes: ["email", "departmentId", "fullname"],
            require: true,
            raw: true,
            include: [
              {
                model: Department,
                as: "department",
                attributes: ["departmentName"],
                require: true,
                raw: true,
              },
            ],
          },
        ],
        attributes: ["id", "userId", "deleted", "announced"],
        raw: true,
      });

      const newRes = res
        .filter((item) => item.deleted === 0)
        .map((item) => {
          return {
            id: item.userId,
            label: item["user.email"],
            deleted: item.deleted,
            fullname: item["user.fullname"],
            announced: item.announced,
            departmentName: item["user.department.departmentName"],
          };
        });
      return newRes;
    } catch (error) {
      throw error;
    }
  },

  async deleteUserOutOfMeeting(req) {
    try {
      const { id } = req.params;
      const { arrUserId, bookingId, userIdLogin } = req.body;

      const bookingDetailUnderData = await BookingInfo.findOne({
        where: { id: bookingId },
        raw: true,
      });

      const checkAdmin = await User.findOne({
        where: {
          id: userIdLogin,
        },
      });

      if (bookingDetailUnderData.status !== "comingUp")
        throw new AppError(400, "Cuộc họp này đã xong hoặc đã bị xóa");

      if (checkAdmin.role === "admin") {
        const res = await Participant.update(
          { deleted: 1 },
          { where: { userId: arrUserId, bookingId: id } }
        );
        return res;
      }

      if (bookingDetailUnderData.userId !== userIdLogin)
        throw new AppError(400, "bạn không phải là chủ đơn nên không được xóa");

      const res = await Participant.update(
        { deleted: 1 },
        { where: { userId: arrUserId, bookingId: id } }
      );
      return res;
    } catch (error) {
      throw error;
    }
  },

  async userLeavesTheMeetingRoom(req) {
    const { id } = req.params;
    const { userId, userIdLogin } = req.body;

    const bookingDetailUnderData = await BookingInfo.findOne({
      where: { id },
      raw: true,
    });

    if (bookingDetailUnderData.status !== "comingUp")
      throw new AppError(400, "cuộc họp này đã xong hoặc đã bị xóa");

    if (bookingDetailUnderData.userId === userIdLogin) {
      throw new AppError(
        (400, "bạn là chủ cuộc họp , nên bạn không thẻ thoát ra khỏi cuộc họp này")
      );
    }

    const res = await Participant.update(
      { deleted: 1 },
      {
        where: {
          bookingId: id,
          userId,
        },
      }
    );
    return res;
  },
};

module.exports = MeetingService;
