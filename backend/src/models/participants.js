module.exports = function (sequelize, DataTypes) {
  const Participant = sequelize.define(
    "Participant",
    {
      id: {
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
      },
      userId: {
        type: DataTypes.INTEGER,
        allowNull: true,
        references: {
          model: "Users",
          key: "id",
        },
      },
      bookingId: {
        type: DataTypes.INTEGER,
        allowNull: true,
        references: {
          model: "BookingInfos",
          key: "id",
        },
      },

      announced: {
        type: DataTypes.BOOLEAN,
        allowNull: true,
        defaultValue: 0,
      },
      deleted: {
        type: DataTypes.BOOLEAN,
        allowNull: true,
        defaultValue: 0,
      },
    },
    {
      sequelize,
      tableName: "Participants",
      timestamps: false,
      indexes: [
        {
          name: "PRIMARY",
          unique: true,
          using: "BTREE",
          fields: [{ name: "id" }],
        },
        {
          name: "userID",
          using: "BTREE",
          fields: [{ name: "userId" }],
        },
        {
          name: "bookingID",
          using: "BTREE",
          fields: [{ name: "bookingId" }],
        },
      ],
    }
  );
  Participant.associate = function (models) {
    Participant.belongsTo(models.BookingInfo, {
      as: "booking",
      foreignKey: "bookingId",
    });
    Participant.belongsTo(models.User, { as: "user", foreignKey: "userId" });
  };
  return Participant;
};
