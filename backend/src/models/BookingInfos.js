module.exports = function (sequelize, DataTypes) {
  const BookingInfo = sequelize.define(
    "BookingInfo",
    {
      id: {
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
      },
      startingTime: {
        type: DataTypes.DATE,
        allowNull: false,
      },
      endingTime: {
        type: DataTypes.DATE,
        allowNull: false,
      },
      departmentId: {
        type: DataTypes.INTEGER,
        allowNull: true,
        references: {
          model: "Departments",
          key: "id",
        },
      },
      meetingContent: {
        type: DataTypes.STRING(250),
        defaultValue: "",
      },
      status: {
        type: DataTypes.ENUM("canceled", "done", "comingUp"),
        allowNull: true,
        defaultValue: "comingUp",
      },
      reason: {
        type: DataTypes.STRING(250),
        allowNull: true,
        defaultValue: "",
      },
      userId: {
        type: DataTypes.INTEGER,
        allowNull: true,
        references: {
          model: "Users",
          key: "id",
        },
      },
      roomId: {
        type: DataTypes.INTEGER,
        allowNull: true,
        references: {
          model: "Rooms",
          key: "id",
        },
      },
      eventId: {
        type: DataTypes.STRING(250),
        allowNull: true,
        defaultValue: "",
      },
    },
    {
      sequelize,
      tableName: "BookingInfos",
      timestamps: false,
      indexes: [
        {
          name: "PRIMARY",
          unique: true,
          using: "BTREE",
          fields: [{ name: "id" }],
        },
        {
          name: "userID",
          using: "BTREE",
          fields: [{ name: "userId" }],
        },
        {
          name: "roomID",
          using: "BTREE",
          fields: [{ name: "roomId" }],
        },
        {
          name: "departmentId",
          using: "BTREE",
          fields: [{ name: "departmentId" }],
        },
      ],
    }
  );
  BookingInfo.associate = function (models) {
    BookingInfo.hasMany(models.Participant, {
      as: "Participants",
      foreignKey: "bookingId",
    });
    BookingInfo.belongsTo(models.Department, {
      as: "department",
      foreignKey: "departmentId",
    });
    BookingInfo.belongsTo(models.Room, { as: "room", foreignKey: "roomId" });
    BookingInfo.belongsTo(models.User, { as: "user", foreignKey: "userId" });
  };
  return BookingInfo;
};
