"use strict";
module.exports = (sequelize, DataTypes) => {
  const Department = sequelize.define(
    "Department",
    {
      id: {
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
      },
      departmentName: {
        type: DataTypes.STRING(250),
        allowNull: false,
      },
      departmentColor: {
        type: DataTypes.STRING(50),
        defaultValue: "#027bff",
      },
      telegramGroupId: {
        type: DataTypes.STRING,
      },
      deleted: {
        type: DataTypes.BOOLEAN,
        allowNull: true,
        defaultValue: 0,
      },
    },
    {
      sequelize,
      tableName: "Departments",
      timestamps: false,
      indexes: [
        {
          name: "PRIMARY",
          unique: true,
          using: "BTREE",
          fields: [{ name: "id" }],
        },
      ],
    }
  );

  Department.associate = function (models) {
    Department.hasMany(models.BookingInfo, {
      as: "BookingInfos",
      foreignKey: "departmentId",
    });
    Department.hasMany(models.User, {
      as: "Users",
      foreignKey: "departmentId",
    });
  };

  return Department;
};
