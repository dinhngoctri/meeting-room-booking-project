require("dotenv").config();
const express = require("express");
const cors = require("cors");
const moment = require("moment");
const bodyParser = require("body-parser");
const routerVer1 = require("./routers/v1/index");
const { handleError } = require("./helpers/error");
const axios = require("axios");
const { TELEGRAM_API, MEETING_BOT_TOKEN, SERVER_URL } = require("./config/telegram");
const logger = require("morgan");

const app = express();
app.use(express.json());
app.use(bodyParser.json());
app.use(cors());
app.use("/", routerVer1);
app.use(handleError);
app.use(logger("dev"));

const { updateBookingStatus } = require("./services/meeting.service");
updateBookingStatus();

// const projectRunningDay = moment().format("YYYY-MM-DD");
const { remindMeetingNotify, updateTelegramId } = require("./services/telegram.service");
// remindMeetingNotify(projectRunningDay);

let isIntervalRunning = false;
setInterval(() => {
  const runningTime = moment().format("HH:mm:ss");
  const today = moment().format("YYYY-MM-DD");
  if (!isIntervalRunning && runningTime > "08:00:01" && runningTime < "18:00:00") {
    updateInterval = setInterval(updateBookingStatus, 1800000);
    isIntervalRunning = true;
  }
  if (isIntervalRunning && (runningTime < "08:00:01" || runningTime >= "18:00:00")) {
    clearInterval(updateInterval);
    isIntervalRunning = false;
  }
  if (runningTime === "08:00:00") {
    remindMeetingNotify(today);
  }
}, 1000);

const URI = `/webhook/${MEETING_BOT_TOKEN}`;
const WEBHOOK_URL = SERVER_URL + URI;

const init = async () => {
  const res = await axios.get(`${TELEGRAM_API}/setWebhook?url=${WEBHOOK_URL}`);
  console.log("res: ", res.data);
};

app.post(URI, async (req, res) => {
  const { message } = req.body;
  console.log("req.body: ", req.body);

  if (message && message.new_chat_member && message.new_chat_member.is_bot) {
    const { id, title } = message.chat;
    const respone = await updateTelegramId(id, title);
    axios.post(`${TELEGRAM_API}/sendMessage`, {
      chat_id: id,
      text: respone,
    });
  }

  if (message && message.left_chat_member && message.left_chat_member.is_bot) {
    const { title } = message.chat;
    await updateTelegramId(null, title);
  }

  return res.send();
});

app.set("view engine", "ejs");

app.listen(process.env.PORT, async () => {
  console.log("process.env.PORT: ", process.env.PORT);
  await init();
});
