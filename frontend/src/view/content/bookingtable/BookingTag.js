import { getMaxCharOfStr } from "../../../core/helpers/stringHandling";

export default function(bookingInfo) {
  const { timeRange, meetingContent } = bookingInfo;

  const stylingObj = {
    width: "100%",
    height: `${60 * timeRange}px`,

    position: "absolute",
    top: "0",
    left: "0",
    "z-index": "1",

    overflow: "scroll",
  };

  let stylingStr = "";

  for (const key in stylingObj) {
    stylingStr += `${key}:${stylingObj[key]};`;
  }

  return `
  <div 
  style="${stylingStr}" 
  class="p-2 bg-primary border rounded text-white"
  >
  <p>${
    meetingContent ? getMaxCharOfStr(meetingContent, 150) : "Không có nội dung cuộc họp"
  }</p>
  </div>
  `;
}
